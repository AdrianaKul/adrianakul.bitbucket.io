#!/bin/sh

RSYNC=/usr/bin/rsync
PROJECT_SOURCE_DIR=/Users/adriana/mofem_install/mofem-cephas/mofem
BINDIR=/Users/adriana/mofem_install/lib_release

$RSYNC -avz --delete html lukasz@cdash.eng.gla.ac.uk:/var/www/html/mofem
