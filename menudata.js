/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"",url:"@ref mainpage"},
{text:"Getting started",url:"usersmain.html",children:[
{text:"Installation",url:"installation.html"},
{text:"Features",url:"motivation.html"},
{text:"Video guides",url:"https://www.youtube.com/playlist?list=PLW49at2nN39gsTI1vWxvHBOjFK0q_h83s"},
{text:"Examples",url:"runningprograms.html"},
{text:"Frequently Asked Questions",url:"faqs.html"},
{text:"Search",url:"google_search.html"}]},
{text:"Developer",url:"developersmain.html",children:[
{text:"Tutorials and examples",url:"developersmain.html#developer_useful_links",children:[
{text:"Basic design",url:"general_data_structure.html"},
{text:"Tutorials",url:"tutorials.html"},
{text:"Examples",url:"examples.html"}]},
{text:"Source code",url:"developersmain.html#developer_source_code",children:[
{text:"Source groups",url:"modules.html"},
{text:"Source files",url:"files.html"},
{text:"Name spaces",url:"namespaces.html"},
{text:"Classes",url:"annotated.html"}]},
{text:"Coding practice",url:"developersmain.html#mofem_practice",children:[
{text:"Coding practice",url:"coding_practice.html"},
{text:"Guidelines for bug reporting",url:"guidelines_bug_reporting.html"},
{text:"Writing documentation",url:"a_guide_to_writing_documentation.html"},
{text:"Being a good citizen",url:"being_a_good_citizen.html"}]},
{text:"Todo, bugs, and other lists",url:"developersmain.html#mofem_management",children:[
{text:"Bug List",url:"bug.html"},
{text:"Todo List",url:"todo.html"},
{text:"Deprecated List",url:"deprecated.html"}]},
{text:"CDash testing",url:"http://cdash.eng.gla.ac.uk/cdash"},
{text:"Bibliography",url:"citelist.html"},
{text:"Related Pages",url:"pages.html"}]},
{text:"Modules",url:"mod_gallery.html"},
{text:"Q&A",url:"https://groups.google.com/forum/#!categories/mofem-group"},
{text:"Repository",url:"https://bitbucket.org/likask/mofem-cephas"},
{text:"Library",url:"https://zenodo.org/communities/mofem/"},
{text:"About",url:"about_us.html"}]}
