var searchData=
[
  ['kelvinvoigtdamper_12386',['KelvinVoigtDamper',['../struct_kelvin_voigt_damper.html#a74d16d48db8489dc14116421a9c8fcdd',1,'KelvinVoigtDamper']]],
  ['kernellobattopolynomial_12387',['KernelLobattoPolynomial',['../struct_mo_f_e_m_1_1_kernel_lobatto_polynomial.html#a884c7a441fabc8670281b8e922e1fa8e',1,'MoFEM::KernelLobattoPolynomial']]],
  ['kernellobattopolynomialctx_12388',['KernelLobattoPolynomialCtx',['../struct_mo_f_e_m_1_1_kernel_lobatto_polynomial_ctx.html#ac699b45cf0b55c672436b481b3c2666f',1,'MoFEM::KernelLobattoPolynomialCtx']]],
  ['kext_5fhh_5fhierarchical_12389',['KExt_hh_hierarchical',['../_surface_pressure_complex_for_lazy_8cpp.html#a43736b7b6e69e059330988e6bee0b8f4',1,'KExt_hh_hierarchical(double eps, int order, int *order_edge, double *N, double *N_face, double *N_edge[], double *diffN, double *diffN_face, double *diffN_edge[], double *t, double *t_edge[], double *t_face, double *dofs_x, double *dofs_x_edge[], double *dofs_x_face, double *KExt_hh, double *KExt_egdeh[3], double *KExt_faceh, int g_dim, const double *g_w):&#160;SurfacePressureComplexForLazy.cpp'],['../_surface_pressure_complex_for_lazy_8cpp.html#a86d34b24f4d3ddbd39463f1892d6aeff',1,'KExt_hh_hierarchical(double eps, int order, int *order_edge, double *N, double *N_face, double *N_edge[], double *diffN, double *diffN_face, double *diffN_edge[], double *t, double *t_edge[], double *t_face, double *dofs_x, double *dofs_x_edge[], double *dofs_x_face, double *KExt_hh, double *KExt_edgeh[], double *KExt_faceh, int g_dim, const double *g_w):&#160;SurfacePressureComplexForLazy.cpp']]],
  ['kext_5fhh_5fhierarchical_5fedge_12390',['KExt_hh_hierarchical_edge',['../_surface_pressure_complex_for_lazy_8cpp.html#ab7eead4da1e29592603723ff111c55bd',1,'SurfacePressureComplexForLazy.cpp']]],
  ['kext_5fhh_5fhierarchical_5fface_12391',['KExt_hh_hierarchical_face',['../_surface_pressure_complex_for_lazy_8cpp.html#ad6e648f425469d68ab835b32eb3b09b7',1,'SurfacePressureComplexForLazy.cpp']]],
  ['keyfromkey_12392',['KeyFromKey',['../struct_mo_f_e_m_1_1_key_from_key.html#a88d1ae3a573245fbd12ee60c514bc2b4',1,'MoFEM::KeyFromKey']]],
  ['ksp_5fset_5foperators_12393',['ksp_set_operators',['../_p_c_m_g_set_up_via_approx_orders_8cpp.html#a98deeb71c047465488fe1636b3587209',1,'PCMGSetUpViaApproxOrders.cpp']]],
  ['kspctx_12394',['KspCtx',['../struct_mo_f_e_m_1_1_ksp_ctx.html#a43c2a178e187f300540c0e16a1df83e9',1,'MoFEM::KspCtx']]],
  ['kspmat_12395',['KspMat',['../namespace_mo_f_e_m.html#aa03637d77d33bee96a5ae23896fb3b3c',1,'MoFEM']]],
  ['kspmethod_12396',['KspMethod',['../struct_mo_f_e_m_1_1_ksp_method.html#ab2102e6208f948ce3081b972cf32a7c9',1,'MoFEM::KspMethod']]],
  ['ksprhs_12397',['KspRhs',['../namespace_mo_f_e_m.html#a70f451d801666b6f73eea59001ff3dc7',1,'MoFEM']]]
];
