var indexSectionsWithContent =
{
  0: "23_abcdefghijklmnopqrstuvwxyz~",
  1: "_abcdefghijklmnopqrstuvwz",
  2: "bcfmopr",
  3: "23abcdefghijklmnopqrstuvwxz",
  4: "_abcdefghijklmnopqrstuvwz~",
  5: "abcdefghijklmnopqrstuvwxyz",
  6: "_abcdefgilmnopqrstuv",
  7: "abcdfhiklmorstv",
  8: "abcdefghijklmnopqrstuv",
  9: "acefkmopstuvz",
  10: "_abcdefghilmnpqrstx",
  11: "abcdefgilmnprstuv",
  12: "abcdefghilmnprstuw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "groups",
  12: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Modules",
  12: "Pages"
};

