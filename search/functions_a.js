var searchData=
[
  ['jacobi_5fpolynomials_12383',['Jacobi_polynomials',['../base__functions_8h.html#a6a3cc1686095ccf11d7381ccb9139bc3',1,'Jacobi_polynomials(int p, double alpha, double x, double t, double *diff_x, double *diff_t, double *L, double *diffL, const int dim):&#160;base_functions.c'],['../base__functions_8c.html#a6a3cc1686095ccf11d7381ccb9139bc3',1,'Jacobi_polynomials(int p, double alpha, double x, double t, double *diff_x, double *diff_t, double *L, double *diffL, const int dim):&#160;base_functions.c']]],
  ['jacobipolynomial_12384',['JacobiPolynomial',['../struct_mo_f_e_m_1_1_jacobi_polynomial.html#ad7b121e1459b9141e3b09b444eebc31c',1,'MoFEM::JacobiPolynomial']]],
  ['jacobipolynomialctx_12385',['JacobiPolynomialCtx',['../struct_mo_f_e_m_1_1_jacobi_polynomial_ctx.html#ab426344e86d21cba8b9af73ceaf2b1ea',1,'MoFEM::JacobiPolynomialCtx']]]
];
