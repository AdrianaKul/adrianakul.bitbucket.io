var searchData=
[
  ['installation_20with_20docker_20_28linux_2c_20macos_20and_20some_20versions_20of_20windows_29_16896',['Installation with Docker (Linux, macOS and some versions of Windows)',['../install_docker.html',1,'installation']]],
  ['installation_20on_20macos_20_28advanced_29_16897',['Installation on macOS (Advanced)',['../install_macosx.html',1,'installation']]],
  ['installation_20with_20spack_20_28recommended_20for_20linux_2c_20macos_2c_20hpc_29_16898',['Installation with Spack (Recommended for Linux, macOS, HPC)',['../install_spack.html',1,'installation']]],
  ['installation_20on_20ubuntu_20_28advanced_29_16899',['Installation on Ubuntu (Advanced)',['../install_ubuntu.html',1,'installation']]],
  ['installation_16900',['Installation',['../installation.html',1,'usersmain']]],
  ['implementing_20operators_20for_20the_20poisson_20equation_16901',['Implementing operators for the Poisson equation',['../poisson_tut2.html',1,'tutorials']]],
  ['implementation_20of_20reaction_2ddiffusion_20equation_16902',['Implementation of reaction-diffusion equation',['../reaction_diffusion_imp.html',1,'tutorials']]],
  ['implementation_20of_20spring_20element_16903',['Implementation of spring element',['../spring_element.html',1,'tutorials']]]
];
